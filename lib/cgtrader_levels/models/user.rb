class CgtraderLevels::User < ActiveRecord::Base
  belongs_to :level
  has_many :benefits, through: :level

  after_initialize do
    if matching_level
      self.level_id = matching_level.id
    end
  end

  before_update :set_new_level, if: :will_save_change_to_reputation?

  private

  def set_new_level
    if matching_level && matching_level.id != level_id
      self.level_id = matching_level.id
      apply_benefits
    end
  end

  def matching_level
    CgtraderLevels::Level.where(experience: ..reputation).order(experience: :desc).first
  end

  def apply_benefits
    benefits.each do |benefit|
      benefit.apply_benefit(self)
    end
  end
end
