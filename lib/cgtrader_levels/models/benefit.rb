class CgtraderLevels::Benefit < ActiveRecord::Base
    has_and_belongs_to_many :levels

    enum benefit_type: { tax_reduction: 0, add_coins: 1 }

    def apply_benefit(user)
        case benefit_type.to_sym
        when :tax_reduction
            user.tax -= value
        when :add_coins
            user.coins += value
        end
    end
end
