require 'active_record'

ActiveRecord::Base.establish_connection(
  adapter: 'sqlite3',
  database: ':memory:',
  verbosity: 'quiet'
)

ActiveRecord::Base.connection.create_table :users do |table|
  table.string :username
  table.integer :reputation, default: 0
  table.decimal :coins, default: 0
  table.decimal :tax, default: 30
  table.references :level
end

ActiveRecord::Base.connection.create_table :levels do |table|
  table.string :title
  table.integer :experience
end

ActiveRecord::Base.connection.create_table :benefits do |table|
  table.integer :benefit_type
  table.integer :value
end

ActiveRecord::Base.connection.create_table :benefits_levels do |table|
  table.references :level
  table.references :benefit
end
