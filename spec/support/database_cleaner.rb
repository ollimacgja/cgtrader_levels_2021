RSpec.configure do |config|
  config.before :each do
    ActiveRecord::Base.connection.tap do |connection|
      %w(users levels benefits benefits_levels).each { |table| connection.execute("DELETE FROM #{table}") }
    end
  end
end
