require 'spec_helper'

describe CgtraderLevels::Benefit do
  let(:user) { CgtraderLevels::User.create! }
  
  describe '#apply_benefit' do
    describe ':tax_reduction' do
      let(:tax_reduction_benefit) {CgtraderLevels::Benefit.create!(benefit_type: :tax_reduction, value: 5)}

      it 'Reduce user tax in 5 points' do      
        expect {
            tax_reduction_benefit.apply_benefit(user)
        }.to change { user.tax }.from(30).to(25)
      end
    end

    describe ':add_coins' do
      let(:add_coin_benefit) {CgtraderLevels::Benefit.create!(benefit_type: :add_coins, value: 10)}
      it 'Add 10 coins to user' do      
        expect {
          add_coin_benefit.apply_benefit(user)
        }.to change { user.coins }.from(0).to(10)
      end
    end
  end
end
