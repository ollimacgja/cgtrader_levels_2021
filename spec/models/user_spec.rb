require 'spec_helper'

describe CgtraderLevels::User do
  let(:user) { CgtraderLevels::User.create! }
  let!(:level_1) { CgtraderLevels::Level.create!(experience: 0, title: 'First level') }

  describe 'new user' do
    it 'has 0 reputation points' do
      expect(user.reputation).to eq(0)
    end

    it "has assigned 'First level'" do
      expect(user.level).to eq(level_1)
    end
  end

  describe 'level up' do    
    let!(:level_2) { CgtraderLevels::Level.create!(experience: 10, title: 'Second level') }
    let!(:level_3) { CgtraderLevels::Level.create!(experience: 13, title: 'Third level') }
    
    it "from 'First level' to 'Second level'" do
      expect {
        user.update_attribute(:reputation, 10)
      }.to change { user.reload.level }.from(level_1).to(level_2)
    end

    it "from 'First level' to 'Third level'" do
      expect {
        user.update_attribute(:reputation, 13)
      }.to change { user.reload.level }.from(level_1).to(level_3)
    end

    describe 'bonuses & privileges' do
      let(:add_coin_benefit) {CgtraderLevels::Benefit.create!(benefit_type: :add_coins, value: 7)}
      let(:tax_reduction_benefit) {CgtraderLevels::Benefit.create!(benefit_type: :tax_reduction, value: 1)}

      before {
        level_2.update_attribute(:benefits, [add_coin_benefit, tax_reduction_benefit])
      }

      describe "from level 1 to level 2" do
        it 'gives 7 coins to user' do    
          expect {
            user.update_attribute(:reputation, 10)
          }.to change { user.reload.coins }.from(0).to(7)
        end
    
        it 'reduces tax rate by 1' do
          expect {
            user.update_attribute(:reputation, 10)
          }.to change { user.reload.tax }.from(30).to(29)
        end
      end

      describe "When still on level 1" do
        it 'expect coins not to change' do
          expect {
            user.update_attribute(:reputation, 9)
          }.not_to change { user.reload.coins }
        end

        it 'expect tax not to change' do
          expect {
            user.update_attribute(:reputation, 9)
          }.not_to change { user.reload.tax }
        end
      end
    end
  end
end
