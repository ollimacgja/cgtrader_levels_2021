require 'spec_helper'

describe CgtraderLevels::Level do
  let(:tax_reduction_benefit) {CgtraderLevels::Benefit.create!(benefit_type: :tax_reduction, value: 5)}
  let(:add_coin_benefit) {CgtraderLevels::Benefit.create!(benefit_type: :add_coins, value: 10)}
  let!(:level) { CgtraderLevels::Level.create!(experience: 0, title: 'First level', benefits: [tax_reduction_benefit, add_coin_benefit]) }

  describe 'Level has benefits' do
    it { expect(level.benefits.count).to eq(2) }

    it 'has the tax reduction benefit' do
      expect(level.benefits).to include(tax_reduction_benefit)
    end
  end
end
